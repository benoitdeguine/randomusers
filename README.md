# 👨🏻‍💻 RandomUsers
This is my technical test for the Lydia job position as iOS Developer. You can find the project on `main` branch.

## 💡 Need to improve
- Offline mode to Core data, I haven't got the time to do it, but think the best for it is to use Core data after getting all the users, and fetch to display the user if we don't have an internet connection. We need to save the date of the insert to use it to order users as all users doesn't have any ID.
- Constraint on the UIImageView, the image is hidden for now

## 🖥 UI
- First time I'm doing the UI programatically, always used the XIB file. I've try my best but it's a little complex.
You can find some UI on my personnal apps:

- [Setlist Concert for Setlist.fm](https://apps.apple.com/us/app/christmas-countdown-widget-21/id1541858661#?platform=iphone)
- [Christmas Countdown Widget 21](https://apps.apple.com/us/app/setlist-concert-for-setlist-fm/id1164020210#?platform=iphone)

## 🗒 Notes
- I'm available and happy to discuss about my project, architecture and tests. Feel free to contact me !
