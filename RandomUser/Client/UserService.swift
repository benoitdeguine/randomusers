//
//  UserService.swift
//  RandomUser
//
//  Created by Benoit on 15/12/2021.
//

import Foundation

class UserService: Client, UserServiceSpec {
    private let host = "randomuser.me"
    private let path = "/api"
        
    var delegate: UserServiceDelegate? = nil

    private func urlBuilder(path: String, params: [String: String] = [:]) -> URL? {
        var urlBuilder = URLComponents()
        urlBuilder.scheme = "https"
        urlBuilder.host = host
        urlBuilder.path = self.path + path
        
        if params.count > 0 {
            var items = [URLQueryItem]()
            for (index, value) in params {
                items.append(URLQueryItem(name: index, value: value))
            }
            urlBuilder.queryItems = items
        }

        return urlBuilder.url
    }
    
    func getUsers(results: Int = 10, page: Int = 1, seed: String?) {
        let path = "/"
        var parameters = [
            "results": "\(results)",
            "page": "\(page)"
        ]
        
        if let seed = seed {
            parameters["seed"] = seed
        }
        
        guard let url = self.urlBuilder(path: path, params: parameters) else {
            print("Error when creating the URL")
            return
        }

        let completionBlock: ((Result<Data, ClientError>) -> ()) = { result in
            switch result {
            case .success(let data):
                self.delegate?.onSuccess(data: data)
            case .failure(let error):
                print(error.localizedDescription)
                self.delegate?.onError(error: error)
            }
        }
        
        self.getData(url, completion: completionBlock)
    }
    
    func getImage(urlImage: String) {
        guard let url = URL(string: urlImage) else {
            return
        }
         
        let completionBlock: ((Result<Data, ClientError>) -> ()) = { result in
            switch result {
            case .success(let data):
                self.delegate?.onSuccess(data: data)
            case .failure(let error):
                print(error.localizedDescription)
                self.delegate?.onError(error: error)
            }
        }
        
        self.getData(url, completion: completionBlock)
    }
    
}
