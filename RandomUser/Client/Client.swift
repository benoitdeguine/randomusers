//
//  Client.swift
//  RandomUser
//
//  Created by Benoit on 15/12/2021.
//

import Foundation

enum ClientError: Error {
    case decode
    case netError(error: Error)
}

enum ClientMethod: String {
    case get = "GET"
}

class Client {
    
    func getData(_ url: URL, method: ClientMethod = .get, completion: @escaping (Result<Data, ClientError>) -> ()) {
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue

        URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion(.failure(.netError(error: error)))
                }
                if let data = data {
                    completion(.success(data))
                }
            }
        }.resume()
    }
}
