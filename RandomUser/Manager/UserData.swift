//
//  USerData.swift
//  RandomUser
//
//  Created by Benoit on 20/12/2021.
//

import Foundation

class UserData {
    
    var users: [User] = []
    let decoder = JSONDecoder()
    
    init(data: Data) {
        do {
            let decoded = try decoder.decode(UserResult.self, from: data)
            self.users = decoded.results
        } catch {
            print("Error when decoding data")
            self.users = []
        }
    }
}
