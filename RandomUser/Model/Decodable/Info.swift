//
//  Info.swift
//  RandomUser
//
//  Created by Benoit on 15/12/2021.
//

import Foundation

struct Info: Decodable {
    var seed: String
    var results: Int
    var page: Int
    var version: String
}
