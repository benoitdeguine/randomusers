//
//  Street.swift
//  RandomUser
//
//  Created by Benoit on 15/12/2021.
//

import Foundation

struct Street: Decodable {
    var number: Int
    var name: String
}
