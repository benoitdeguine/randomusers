//
//  Name.swift
//  RandomUser
//
//  Created by Benoit on 15/12/2021.
//

import Foundation

struct Name: Decodable {
    var title: String
    var first: String
    var last: String
}
