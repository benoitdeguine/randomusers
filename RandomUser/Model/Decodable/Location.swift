//
//  Location.swift
//  RandomUser
//
//  Created by Benoit on 15/12/2021.
//

import Foundation

class Location: Decodable {
    
    var street: Street
    var city: String
    var state: String
    var country: String
    var postcode: String
    var coordinates: Coordinates
    
    enum CodingKeys: String, CodingKey {
        case street, city, state, country, postcode, coordinates, timezone
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        if let postcode = try? values.decode(Int.self, forKey: .postcode) {
            self.postcode = "\(postcode)"
        } else {
            self.postcode = try! values.decode(String.self, forKey: .postcode)
        }
        self.street = try values.decode(Street.self, forKey: .street)
        self.city = try values.decode(String.self, forKey: .city)
        self.state = try values.decode(String.self, forKey: .state)
        self.country = try values.decode(String.self, forKey: .country)
        self.coordinates = try values.decode(Coordinates.self, forKey: .coordinates)
    }
}
