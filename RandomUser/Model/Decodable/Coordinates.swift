//
//  Coordinates.swift
//  RandomUser
//
//  Created by Benoit on 15/12/2021.
//

import Foundation

struct Coordinates: Decodable {
    var latitude: String
    var longitude: String
}
