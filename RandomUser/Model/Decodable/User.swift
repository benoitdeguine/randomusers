//
//  User.swift
//  RandomUser
//
//  Created by Benoit on 15/12/2021.
//

import Foundation

class User: Decodable {
    var gender: Gender
    var name: Name
    var location: Location
    var email: String
    var phone: String
    var picture: String
    
    enum CodingKeys: String, CodingKey {
        case gender, name, location, email, phone, picture
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        let gender = try? values.decode(String.self, forKey: .gender)
        switch gender {
        case "male":
            self.gender = .male
        case "female":
            self.gender = .female
        default:
            self.gender = .unknown
        }
        
        self.name = try values.decode(Name.self, forKey: .name)
        self.location = try values.decode(Location.self, forKey: .location)
        self.email = try values.decode(String.self, forKey: .email)
        self.phone = try values.decode(String.self, forKey: .phone)
        self.picture = try values.decode(Picture.self, forKey: .picture).medium
    }
}


