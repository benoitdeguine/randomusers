//
//  Picture.swift
//  RandomUser
//
//  Created by Benoit on 15/12/2021.
//

import Foundation

struct Picture: Decodable {
    var large: String
    var medium: String
    var thumbnail: String
}
