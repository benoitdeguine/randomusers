//
//  Gender.swift
//  RandomUser
//
//  Created by Benoit on 15/12/2021.
//

import Foundation

enum Gender: String {
    case female
    case male
    case unknown
}
