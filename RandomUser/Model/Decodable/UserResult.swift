//
//  UserResult.swift
//  RandomUser
//
//  Created by Benoit on 15/12/2021.
//

import Foundation

struct UserResult: Decodable {
    var results: [User]
    var info: Info
}
