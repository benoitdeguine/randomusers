//
//  UserDetailPhoneItem.swift
//  RandomUser
//
//  Created by Benoit on 17/12/2021.
//

import Foundation

struct UserDetailPhoneItem: UserDetailItem, SelectableView {
    var phone: String
}
