//
//  UserListItem.swift
//  RandomUser
//
//  Created by Benoit on 15/12/2021.
//

import Foundation

struct UserListItem {
    var name: String
    var email: String
}
