//
//  UserDetailAvatarItem.swift
//  RandomUser
//
//  Created by Benoit on 17/12/2021.
//

import Foundation

struct UserDetailAvatarItem: UserDetailItem {
    var url: String
}
