//
//  UserDetailEmailItem.swift
//  RandomUser
//
//  Created by Benoit on 17/12/2021.
//

import Foundation

struct UserDetailEmailItem: UserDetailItem, SelectableView {
    var email: String
}
