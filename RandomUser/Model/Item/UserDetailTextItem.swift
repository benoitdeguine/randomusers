//
//  UserDetailTextItem.swift
//  RandomUser
//
//  Created by Benoit on 17/12/2021.
//

import Foundation

struct UserDetailTextItem: UserDetailItem {
    var key: String
    var value: String
}
