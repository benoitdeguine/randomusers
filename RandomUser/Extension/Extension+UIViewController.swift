//
//  Extension+UIViewController.swift
//  RandomUser
//
//  Created by Benoit on 16/12/2021.
//

import Foundation
import UIKit

extension UIViewController {
    
    func showAlert(title: String, message: String, textConfirmButton: String? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: textConfirmButton ?? "alert_error_confirm_button".localized, style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}
