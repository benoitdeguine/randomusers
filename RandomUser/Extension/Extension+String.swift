//
//  Extension+String.swift
//  RandomUser
//
//  Created by Benoit on 16/12/2021.
//

import Foundation

extension String {
    
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
}
