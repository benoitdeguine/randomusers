//
//  Extension+Data.swift
//  RandomUser
//
//  Created by Benoit on 15/12/2021.
//

import Foundation

extension Data {

    func decoded<T: Decodable>() throws -> T {
        return try JSONDecoder().decode(T.self, from: self)
    }

}
