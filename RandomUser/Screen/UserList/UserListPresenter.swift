//
//  UserListPresenter.swift
//  RandomUser
//
//  Created by Benoit on 15/12/2021.
//

import Foundation

class UserListPresenter {

    weak private var view: UserListView?
    private var userServiceSpec: UserServiceSpec
    private var users: [User] = []
    private var seed: String? = "30645512f7e3a778"
    
    init(userService: UserServiceSpec) {
        self.userServiceSpec = userService
        self.userServiceSpec.delegate = self
    }
    
    func attachView(view: UserListView) {
        self.view = view
    }
    
    func convertToItem(users: [User]) -> [UserListItem] {
       return users.map {
            return UserListItem(name: "\($0.name.title) \($0.name.first) \($0.name.last)", email: $0.email)
        }
    }
    
    func showDetails(index: Int) {
        let user = self.users[index]
        self.view?.openDetails(user: user)
    }
    
    func getUsers() {
        let total = 30
        self.userServiceSpec.getUsers(results: total, page: (self.users.count / total) + 1, seed: self.seed)
    }
    
    @objc func refreshStatus() {
        self.seed = nil
        self.users = []
        self.view?.refreshUsers()
        self.view?.startLoading()
        self.getUsers()
    }
}

extension UserListPresenter: UserServiceDelegate {
    
    func onError(error: ClientError) {
        self.view?.showError(text: error.localizedDescription)
    }
    
    func onSuccess(data: Data) {
        let userData = UserData(data: data)
        if userData.users.count > 0 {

            let items = self.convertToItem(users: userData.users)
            
            if self.users.isEmpty {
                self.view?.stopLoading()
            }
            self.users += userData.users
            self.view?.setUsers(users: items)
        }
    }
}
