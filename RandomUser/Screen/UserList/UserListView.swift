//
//  UserListView.swift
//  RandomUser
//
//  Created by Benoit on 17/12/2021.
//

import Foundation

protocol UserListView: NSObjectProtocol {
    var alreadyLoading: Bool {get set}
    var users: [UserListItem] {get set}
    
    func startLoading()
    func stopLoading()
    func setUsers(users: [UserListItem])
    func openDetails(user: User)
    func showError(text: String)
    func refreshUsers()
}
