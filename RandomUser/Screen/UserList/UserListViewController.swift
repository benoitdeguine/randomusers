//
//  UsersListViewController.swift
//  RandomUser
//
//  Created by Benoit on 15/12/2021.
//

import UIKit

class UserListViewController: UIViewController {
    
    var users = [UserListItem]()
    
    private var activityView: UIActivityIndicatorView?
    private let tableView = UITableView()
    private var safeArea: UILayoutGuide!
    
    var presenter: UserListPresenter?
    internal var alreadyLoading: Bool = false
    
    override func loadView() {
        super.loadView()
        
        self.view.backgroundColor = .white
        self.safeArea = view.layoutMarginsGuide
        
        self.setupActivityIndicator()
        self.setupTableView()
        self.addRightBarButtomItem()
        
        self.presenter?.attachView(view: self)
        self.presenter?.getUsers()
    }
    
    private func setupActivityIndicator() {
        if #available(iOS 13.0, *) {
            self.activityView = UIActivityIndicatorView(style: .large)
        } else {
            self.activityView = UIActivityIndicatorView(style: .gray)
        }
        self.activityView?.center = self.view.center
        
        if let activityView = activityView {
            self.view.addSubview(activityView)
            self.activityView?.startAnimating()
        }
    }
    
    private func addRightBarButtomItem() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "refresh".localized, style: .plain, target: self, action: #selector(btnAction))
    }
    
    @objc func btnAction() {
        self.presenter?.refreshStatus()
    }
    
    func setupTableView() {
        view.addSubview(self.tableView)

        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.isHidden = true
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        self.tableView.topAnchor.constraint(equalTo: safeArea.topAnchor).isActive = true
        self.tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        self.tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        self.tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.size.height && !self.alreadyLoading {
            self.alreadyLoading = true
            self.presenter?.getUsers()
        }
    }
}

extension UserListViewController: UserListView {

    func showError(text: String) {
        self.showAlert(title: "", message: text, textConfirmButton: nil)
    }
    
    func openDetails(user: User) {
        let viewController = UserDetailViewController()
        let presenter = UserDetailPresenter(user: user, userService: UserService())
        presenter.attachView(view: viewController)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func startLoading() {
        self.activityView?.startAnimating()
    }
    
    func stopLoading() {
        self.activityView?.stopAnimating()
        self.tableView.isHidden = false
    }
    
    func setUsers(users: [UserListItem]) {
        self.users += users
        self.alreadyLoading = false
        self.tableView.reloadData()
    }
    
    func refreshUsers() {
        self.users = []
        self.alreadyLoading = false
        self.tableView.isHidden = true
        self.tableView.reloadData()
    }
}

extension UserListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
        let user = self.users[indexPath.row]
        cell.textLabel?.text = user.name
        cell.detailTextLabel?.text = user.email
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.presenter?.showDetails(index: indexPath.row)
    }
}
