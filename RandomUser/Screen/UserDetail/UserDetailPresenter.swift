//
//  UserDetailsPresenter.swift
//  RandomUser
//
//  Created by Benoit on 16/12/2021.
//

import Foundation

class UserDetailPresenter {
    
    var user: User
    var userService: UserServiceSpec
    weak private var view: UserDetailView?
    
    init(user: User, userService: UserServiceSpec) {
        self.user = user
        self.userService = userService
        self.userService.delegate = self
        self.userService.getImage(urlImage: user.picture)
    }

    func attachView(view: UserDetailView) {
        self.view = view
        self.convertUserToItem()
    }
    
    func convertUserToItem() {
        var items: [UserDetailItem] = []
        
        items.append(UserDetailAvatarItem(url: user.picture))
        items.append(UserDetailTextItem(key: "first_name".localized, value: user.name.first))
        items.append(UserDetailTextItem(key: "last_name".localized, value: user.name.last))
        items.append(UserDetailEmailItem(email: user.email))
        items.append(UserDetailPhoneItem(phone: user.phone))
        items.append(UserDetailTextItem(key: "address".localized, value: "\(user.location.street.number)  \(user.location.street.name) \n\(user.location.postcode) \(user.location.city)"))

        self.view?.setItems(items: items)
    }
}

extension UserDetailPresenter: UserServiceDelegate {
    
    func onSuccess(data: Data) {
        self.view?.refreshAvatarImage(data: data)
    }
    
    func onError(error: ClientError) {
        print("Error when downloading user image - Send log")
    }
}
