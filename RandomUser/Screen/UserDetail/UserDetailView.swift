//
//  UserDetailView.swift
//  RandomUser
//
//  Created by Benoit on 17/12/2021.
//

import Foundation

protocol UserDetailView: NSObjectProtocol {
    var items: [UserDetailItem] {get set}

    func setItems(items: [UserDetailItem])
    func refreshAvatarImage(data: Data)
}
