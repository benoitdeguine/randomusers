//
//  UserDetailViewController.swift
//  RandomUser
//
//  Created by Benoit on 16/12/2021.
//

import UIKit

class UserDetailViewController: UIViewController {
    
    var safeArea: UILayoutGuide!
    let tableView = UITableView()
    
    var items = [UserDetailItem]()
    var presenter: UserDetailPresenter?
    var avatarDelegate: UserAvatarDelegate? = nil

    override func loadView() {
        super.loadView()
        
        self.view.backgroundColor = .white
        self.safeArea = view.layoutMarginsGuide
        
        setupTableView()
    }
    
    func setupTableView() {
        view.addSubview(self.tableView)

        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        self.tableView.topAnchor.constraint(equalTo: safeArea.topAnchor).isActive = true
        self.tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        self.tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        self.tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tableView.register(UserAvatarTableViewCell.self, forCellReuseIdentifier: "cellAvatar")
    }
    
}
extension UserDetailViewController: UserDetailView {
    
    func refreshAvatarImage(data: Data) {        
        self.avatarDelegate?.changeAvatar(data: data)
    }
    
    func setItems(items: [UserDetailItem]) {
        self.items = items
        self.tableView.reloadData()
    }
}

extension UserDetailViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.items[indexPath.row]
        switch item {
        case is UserDetailAvatarItem:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellAvatar", for: indexPath) as? UserAvatarTableViewCell
            cell?.addImage()
            self.avatarDelegate = cell
            return cell!
        case is UserDetailEmailItem, is UserDetailPhoneItem, is UserDetailTextItem:
            let cell = UITableViewCell(style: .value2, reuseIdentifier: "cell")

            if let item = item as? UserDetailTextItem {
                cell.textLabel?.text = item.key
                cell.detailTextLabel?.text = item.value
            } else if let item = item as? UserDetailPhoneItem {
                cell.textLabel?.text = "phone".localized
                cell.detailTextLabel?.text = item.phone
            } else if let item = item as? UserDetailEmailItem {
                cell.textLabel?.text = "email".localized
                cell.detailTextLabel?.text = item.email
            }
            cell.detailTextLabel?.numberOfLines = 0
            return cell
        default:
            let cell = UITableViewCell(style: .value2, reuseIdentifier: "cell")

            if let row = self.items[indexPath.row] as? UserDetailTextItem {
                cell.textLabel?.text = row.key
                cell.detailTextLabel?.text = row.value
            }
            return cell
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let row = self.items[indexPath.row]
        return (row is UserDetailAvatarItem) ? 150 : UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = self.items[indexPath.row]
        if row is SelectableView {
            switch row {
            case is UserDetailPhoneItem:
                // Call number
                break
            case is UserDetailEmailItem:
                // Open email
                break
            default:
                // Nothing
                break
            }
        }
    }
}
