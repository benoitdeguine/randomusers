//
//  UserAvatarTableViewCell.swift
//  RandomUser
//
//  Created by Benoit on 16/12/2021.
//

import UIKit

protocol UserAvatarDelegate {
    func changeAvatar(data: Data)
}

class UserAvatarTableViewCell: UITableViewCell {
    
    var delegate: UserAvatarDelegate? = nil
    var avatarImage: RoundedImageView = {
        var imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = imageView.frame.size.width / 2
        
        return RoundedImageView(image: imageView.image)
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func addImage() {
        self.addSubview(avatarImage)

        avatarImage.topAnchor.constraint(equalTo: self.contentView.topAnchor).isActive = true
        avatarImage.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor).isActive = true
        avatarImage.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor).isActive = true
        avatarImage.widthAnchor.constraint(equalToConstant: 100).isActive = true
//        avatarImage.heightAnchor.constraint(equalToConstant: 100).isActive = true

        avatarImage.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        avatarImage.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }
}

extension UserAvatarTableViewCell: UserAvatarDelegate {
    func changeAvatar(data: Data) {
        // Need to fix the Image constraint
//        self.avatarImage.image = UIImage(data: data)
    }
}
