//
//  RandomUserServiceDelegate.swift
//  RandomUser
//
//  Created by Benoit on 17/12/2021.
//

import Foundation

protocol UserServiceDelegate {
    func onError(error: ClientError)
    func onSuccess(data: Data)
}

protocol UserServiceSpec {
    var delegate: UserServiceDelegate? {get set}
    
    func getUsers(results: Int, page: Int, seed: String?)
    func getImage(urlImage: String)
}
