//
//  MockUserDetailView.swift
//  RandomUserTests
//
//  Created by Benoit on 17/12/2021.
//

import Foundation

class MockUserDetailView: NSObject, UserDetailView {
    
    var items: [UserDetailItem] = []
    
    func setItems(items: [UserDetailItem]) {
        self.items = items
    }
    
    func refreshAvatarImage(data: Data) {
        
    }
    
}
