//
//  MockUserListView.swift
//  RandomUserTests
//
//  Created by Benoit on 17/12/2021.
//

import Foundation

class MockUserListView: NSObject, UserListView {
 
    
    
    var alreadyLoading: Bool = false
    var users: [UserListItem] = []
    
    func setUsers(users: [UserListItem]) {
        self.users = users
    }
    func startLoading() {}
    func stopLoading() {}
    func openDetails(user: User) {}
    func showError(text: String) {}
    func refreshUsers() {}
}
