//
//  MockUserService.swift
//  RandomUserTests
//
//  Created by Benoit on 18/12/2021.
//

import Foundation

class MockUserService: UserServiceSpec {
    
    var delegate: UserServiceDelegate?
    
    func getUsers(results: Int, page: Int, seed: String?) {
        
    }
    func getImage(urlImage: String) {
        
    }
}
