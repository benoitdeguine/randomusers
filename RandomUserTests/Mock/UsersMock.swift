//
//  UsersMock.swift
//  RandomUserTests
//
//  Created by Benoit on 17/12/2021.
//

import Foundation

class UsersMock {
    
    var users: [User] = []
    
    init() {
        guard let json = Bundle.main.url(forResource: "users", withExtension: "json") else {
            return
        }
        
        do {
            let decoder = JSONDecoder()
            let dataJson = try Data(contentsOf: json)
            let result = try decoder.decode(UserResult.self, from: dataJson)
            self.users = result.results
        } catch DecodingError.keyNotFound(let key, let context) {
            print("could not find key \(key) in JSON: \(context.debugDescription)")
        } catch DecodingError.valueNotFound(let type, let context) {
            print("could not find type \(type) in JSON: \(context.debugDescription)")
        } catch DecodingError.typeMismatch(let type, let context) {
            print("type mismatch for type \(type) in JSON: \(context.debugDescription)")
        } catch DecodingError.dataCorrupted(let context) {
            print("data found to be corrupted in JSON: \(context.debugDescription)")
        } catch let error as NSError {
            print("Error in read(from:ofType:) domain= \(error.domain), description= \(error.localizedDescription)")
        }
    }
}
