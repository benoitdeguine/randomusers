//
//  UserTest.swift
//  RandomUserTests
//
//  Created by Benoit on 15/12/2021.
//

import XCTest
@testable import RandomUser

class UserTest: XCTestCase {
    
    var users: [User] = []

    override func setUp() {
        self.users = UsersMock().users
    }
    
    func testUsers() {
        XCTAssertEqual(self.users.count, 30)
    }
    
    func testFirstUser() {
        guard let user = self.users.first else {
            XCTFail()
            return
        }
        XCTAssertEqual(user.email, "margarita.soler@example.com")
        XCTAssertEqual(user.name.first, "Margarita")
        XCTAssertEqual(user.name.last, "Soler")
        XCTAssertEqual(user.location.city, "Hospitalet de Llobregat")
        XCTAssertTrue(user.gender == .female)
        XCTAssertEqual(user.picture, "https://randomuser.me/api/portraits/med/women/49.jpg")
    }

}
