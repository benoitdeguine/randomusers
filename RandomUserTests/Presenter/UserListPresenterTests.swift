//
//  UserListPresenterTests.swift
//  RandomUserTests
//
//  Created by Benoit on 17/12/2021.
//

import XCTest
@testable import RandomUser

class UserListPresenterTests: XCTestCase {
    
    let view = MockUserListView()
    let users = UsersMock().users
    var mockUserService: MockUserService?
    var presenter: UserListPresenter?
    
    override func setUp() {
        self.mockUserService = MockUserService()
        self.presenter = UserListPresenter(userService: mockUserService!)
    }

    func testConvertToItems() {
        XCTAssertEqual(view.users.count, 0)
        
        guard let usersItems = presenter?.convertToItem(users: self.users) else { return XCTFail() }
        
        self.view.setUsers(users: usersItems)
        XCTAssertEqual(view.users.count, 30)
    }
    
}
