//
//  UserDetailsPresenterTests.swift
//  RandomUserTests
//
//  Created by Benoit on 17/12/2021.
//

import XCTest
@testable import RandomUser

class UserDetailsPresenterTests: XCTestCase {

    let view = MockUserDetailView()
    let users = UsersMock().users
    let userService: UserServiceSpec = MockUserService()
    
    func testConvertToItems() {
        let presenter: UserDetailPresenter = UserDetailPresenter(user: users.first!, userService: userService)
        presenter.attachView(view: view)
        
        XCTAssertEqual(view.items.count, 6)
        XCTAssertTrue(view.items.first! is UserDetailAvatarItem)
        XCTAssertTrue(view.items.last! is UserDetailTextItem)
    }

}
